class qz_php70fpmconfig {

	file {'phpfpm_conf_setting':
		ensure	=>	present,
		path	=>	'/etc/php/7.0/fpm/php-fpm.conf',
		content	=>	join ([
		'[global]',
		'pid = /run/php/php7.0-fpm.pid',
		'error_log = /var/log/php/php7.0-fpm.log',
		'log_level =  warning',
		'emergency_restart_threshold = 4',
		'emergency_restart_interval = 60s',
		'process_control_timeout = 90s',
		'include=/etc/php/7.0/fpm/pool.d/*.conf',
		''],"\n"),
		owner	=>	'root',
		group	=>	'root',
		mode	=>	'644',
	}		
	file {'phpfpm_logdir_setting':
                ensure  =>      'directory',
                path    =>      '/var/log/php',
                owner   =>      'www-data',
                group   =>      'www-data',
                mode    =>      '0644',
	}
	file {'phpfpm_log_setting':
                ensure  =>      present,
                path    =>      '/var/log/php/php7.0-fpm.log',
                owner   =>      'www-data',
                group   =>      'www-data',
                mode    =>      '0644',
                content	=>	'',
	}
	file {'php_pool_setting':
		ensure	=>	present,
		path	=>	'/etc/php/7.0/fpm/pool.d/www.conf',
		content	=>	join ([
		'[www]',
		'user = www-data',
		'group = www-data',
		'listen = 127.0.0.1:9000',
		'listen.owner = www-data',
		'listen.group = www-data',
		'listen.allowed_clients = 127.0.0.1',
		'pm = dynamic',
		'pm.max_children = 8',
		'pm.start_servers = 2',
		'pm.min_spare_servers = 1',
		'pm.max_spare_servers = 3',
		'pm.max_requests = 5000',
		'slowlog = /var/log/php/$pool.log.slow',
		'request_slowlog_timeout = 10s',
		'request_terminate_timeout = 60s',
		''],"\n"),
		owner	=>	'root',
		group	=>	'root',
		mode	=>	'644',
	}		
	service {'php7.0-fpm':
		ensure	=>	running,
		enable	=>	true,
	}
}
