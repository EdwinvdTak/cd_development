class qz_kernel {
	file_line {'sysctl_setup_swappiness':
		ensure	=>	present,
		path	=>	'/etc/sysctl.conf',
		line	=>	'vm.swappiness=1',
	}
	exec {'exec_sysctl_setup_swapiness':
		command	=> '/sbin/sysctl -p /etc/sysctl.conf',
		provider	=>	shell,
	}
}