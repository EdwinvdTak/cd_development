class qz_checkall {
	# check all facts that need to be checked for proper deployment
	# service nginx
	service {'nginx_run_test':
		name	=> 	'nginx',
		ensure  => 	'running',
		start	=> 	'/sbin/service nginx start',
		stop	=>	'/sbin/service nginx restart',
                restart	=>	'/sbin/service nginx restart; /usr/local/bin/mailme-service.sh phpfpm', 
		pattern	=>	'nginx: master process /usr/sbin/nginx',
		hasrestart	=>	true,
	}
	service {'phpfpm':
		name    =>      'php-fpm',
                ensure  =>      'running',
                start   =>      '/sbin/service php-fpm start',
                stop    =>      '/sbin/service php-fpm stop',
                restart	=>	'/sbin/service php-fpm restart; /usr/local/bin/mailme-service.sh phpfpm', 
                pattern =>      'php-fpm: master process',
                hasrestart	=>	true,
	}
	service {'mariaDB':
		name	=>	'mysql',
/*		ensure	=>	'running', */
	}
}
