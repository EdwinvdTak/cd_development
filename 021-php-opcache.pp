class qz_php70opcache {
        include apt
	apt::ppa { 'ppa:ondrej/php': }
	$packageList = [
		'php7.0-opcache',
	]
	package { $packageList: 
		ensure	=>	'present',
	}
}
