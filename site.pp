# site.pp

node 'default' {
#	include "qz_forceaptipv4"
#	include "qz_disableipv6kernel"
	include "qz_php70install"
	include "qz_php70fpmconfig"
	include "qz_nginxinstall"
	include "qz_mariadbinstall"
	include "all_facts_file"
}

node 'vps-02' {
	include "qz_generiek"
	include "qz_accounts"
	include "qz_postfix"
	include "qz_github"
	include "qz_kernel"
	include "qz_resolver"
}


class all_facts_file {
	file { '/tmp/facts.yaml':
    		content => inline_template("<%= scope.to_hash.reject { |k,v| !( k.is_a?(String) && v.is_a?(String) ) }.to_yaml %>\n"),
	}
	notify {'repository_url':
		message	=> "${qz_config[2]['repo_scheme']}${qz_config[2]['repo_hostname']}${qz_config[2]['repo_uribase']}${qz_config[2]['repo_username']}${qz_config[2]['repo_namedivider']}${qz_config[2]['repo_name']}",
	}
}


