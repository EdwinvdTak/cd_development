
export REPO_PP_URL="https://EdwinvdTak@bitbucket.org/EdwinvdTak/cd_development.git"
export REPO_CFG_URL="https://EdwinvdTak@bitbucket.org/EdwinvdTak/cd_development.git"

#onder deze regel geen instellingen te wijzigen

apt-get -y install python-software-properties
apt-get -y install software-properties-common

apt-get -y install git 
apt-get -y install curl wget 

export rel_codename=`lsb_release -a 2>&1 | grep Codename | cut -d ":" -f 2| sed -e 's/\t//gi'`

wget https://apt.puppetlabs.com/puppetlabs-release-pc1-${rel_codename}.deb
dpkg -i puppetlabs-release-pc1-{rel_codename}.deb
apt-get -y update

apt-get -y install puppet 

puppet module install puppetlabs-apt
puppet module install example42-firewall 
puppet module install example42-mariadb 
puppet module install example42-stdmod
puppet module install puppetlabs-ntp
puppet module install puppetlabs-stdlib
puppet module install rgevaert-pwgen
puppet module install akumria-nullmailer
puppet module install camptocamp-augeas
puppet module install camptocamp-postfix
puppet module install deric-accounts
puppet module install deric-gpasswd
puppet module install example42-iptables
puppet module install example42-puppi
puppet module install example42-resolver
puppet module install jfryman-nginx
puppet module install puppet-staging
puppet module install puppetlabs-concat
puppet module install puppetlabs-mysql

# puppet module install maxchk-varnish

mkdir -p /etc/puppetlabs/puppet/manifests
git clone ${REPO_URL}

export PATH="$PATH:/opt/puppetlabs/bin"

puppet apply /etc/puppetlabs/puppet/manifest/

#eternally puppet in control 

exit