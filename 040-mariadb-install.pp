class qz_mariadbinstall {
        include apt
	apt::ppa { 'ppa:ondrej/mariadb-10.0': }
	apt::key { 'ondrej':
 		id      => '0x14aa40ec0831756756d7f66c4f4ea0aae5267a6c',
  		server  => 'keyserver.ubuntu.com',
  		options => '',
	}
	class mariadb {
    		package { 'MariaDB-server':
        		ensure => installed,
    		}
    	}
	service { 'mysql':
        	ensure => running,
	        enable => true,
	        subscribe => File['/etc/mysql/conf.d/my.cnf'],
    	}
   	file { '/etc/mysql/conf.d/my.cnf':
 		ensure =>	file,
      		mode   =>	'644',
		source =>	'puppet:///storage/mariadb/my.cnf',
	}
}
