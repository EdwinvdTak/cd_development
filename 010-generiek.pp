class qz_generiek {
	# add the duser devops
	
	# set the devops password 
	
	# set the root password 
	
	# make sure the resolver is under management 
	
	# make shure the mailme.sh is here 
	file { 'create_mailme.sh':
		path	=>	'/usr/local/bin/mailme.sh',
		ensure	=>	'file',
		owner	=>	'root',
		group	=>	'root',
		mode	=>	'755',
	  	content	=>	"#!/bin/bash\n\ndmesg | /usr/bin/mail -s \"Hmm, `hostname --fqdn` is opnieuw opgestart!!\" logging@quadzero.nl;\nexit 0\n\n",
	}
	file { 'create_mailme-service.sh':
		path	=>	'/usr/local/bin/mailme-service.sh',
		ensure	=>	'file',
		owner	=>	'root',
		group	=>	'root',
		mode	=>	'755',
	  	content	=>	"#!/bin/bash\n\ndmesg | /usr/bin/mail -s \"Hmm, `hostname --fqdn` heeft een probleem met een service\" logging@quadzero.nl;\nexit 0\n\n",
	}
	# make mailme.sh in rc.local
	file_line { 'rc_local_mailme_add': 
		path	=>	'/etc/rc.local',
	 	line	=>	"/usr/local/bin/mailme.sh\n\nexit 0",
		match 	=>	'^exit 0',
	}

	ensure_packages(
		['git-man',
		'tig',
		'tofrodos',
		'dosfstools',
		'joe',
		'vim',
		'mtr',
		'telnet',
		'curl',
		'iftop',
		'iotop',
		'ifstat',
		'sysstat',
		'dstat',
		'nicstat',
		'atop',
		'unzip',
		'ntpdate',
		'software-properties-common'],
		{'ensure' => 'present'},
	)
        file_line{ 'edit_sysstat': 
		path	=>	'/etc/default/sysstat',
		line	=> 	'ENABLED="true"',
		match 	=>	'ENABLED="false"',
	}
	class { 'ntp':
 	  servers => ['0.ubuntu.pool.ntp.org'],
	}
}

class qz_forceaptipv4 {
	file {'set_apt-to_use_ipv4':
		ensure	=>	present,
		content => 	'Acquire::ForceIPv4 "true";',
		path	=>	'/etc/apt/apt.conf.d/99force-ipv4',
	}
}

class qz_disableipv6kernel {
	file_line { 'disable_ipv6_kernel': 
		path	=>	'/etc/sysctl.conf',
	 	line	=>	"net.ipv6.conf.all.disable_ipv6=1\nnet.ipv6.conf.default.disable_ipv6=1\nnet.ipv6.conf.lo.disable_ipv6=1\n\n",
		match 	=>	'^net.ipv6.conf.all.disable_ipv6=1',
	}
}

