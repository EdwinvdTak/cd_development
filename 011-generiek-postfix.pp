class qz_postfix {
	include postfix
	postfix::config {
		'debug_peer_level':	value	=>'2';
		'debug_peer_list':	value	=>'quadzero.nl';
		'default_process_limit':	value	=>'20';
		'smtpd_helo_required':	value	=>'yes';
		'smtpd_recipient_limit':	value	=>'250';
		'strict_rfc821_envelopes':	value	=>'yes';
		'smtpd_banner':	value	=>'$myhostname ESMTP';
		'biff':	value	=>'no';
		'append_dot_mydomain':	value	=>'no';
		'delay_warning_time':	value	=>'4h';
		'readme_directory':	value	=>'no';
		'smtpd_tls_cert_file':	value	=>'/etc/ssl/certs/ssl-cert-snakeoil.pem';
		'smtpd_tls_key_file':	value	=>'/etc/ssl/private/ssl-cert-snakeoil.key';
		'smtpd_use_tls':	value	=>'yes';
		'smtpd_tls_session_cache_database':	value	=>'btree:${data_directory}/smtpd_scache';
		'smtp_tls_session_cache_database':	value	=>'btree:${data_directory}/smtp_scache';
		'myhostname':	value	=>$::hostname;
		'mydomain':	value	=>$::fqdn;
		'alias_database':	value	=>'hash:/etc/aliases';
#		'myorigin':	value	=>'/etc/mailname';
		'mydestination':	value	=>'$mydomain,$myhostname,localhost.$mydomain,localhost';
		'relayhost':	value	=>'localhost';
		'relay_domains':	value	=>'$mydestination';
		'mynetworks':	value	=>'127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128';
		'mynetworks_style':	value	=>'subnet';
		'mailbox_size_limit':	value	=>'0';
		'message_size_limit':	value	=>'16384000';
		'recipient_delimiter':	value	=>'+';
#		'inet_interfaces':	value	=>'localhost';
		'inet_protocols':	value	=>'all';
		'smtpd_sender_restrictions':	value	=>'permit_mynetworks,permit_sasl_authenticated,reject_non_fqdn_sender,reject_unknown_sender_domain,warn_if_rejectreject_unverified_sender';
		'smtpd_recipient_restrictions':	value	=>'permit_mynetworks,permit_sasl_authenticated,reject_non_fqdn_recipient,reject_unauth_destination,reject_unlisted_recipient,reject_unknown_recipient_domain,reject_unverified_recipient,reject_rbl_clientsbl.spamhaus.org,reject_rbl_clientdnsbl.sorbs.net,check_policy_service inet:127.0.0.1:10023 permit';
		'smtpd_helo_restrictions':	value	=>'permit_mynetworks,reject_invalid_helo_hostname,reject_non_fqdn_helo_hostname,reject_unknown_helo_hostname';
		'unverified_recipient_reject_code':	value	=>'550';
		'unverified_sender_reject_code':	value	=>'550';
		'unknown_address_reject_code':	value	=>'550';
		'unknown_client_reject_code':	value	=>'550';
		'unknown_hostname_reject_code':	value	=>'550';
		'smtp_mx_session_limit':	value	=>'5';
		'bounce_queue_lifetime':	value	=>'3d';
		'maximal_queue_lifetime':	value	=>'3d';
		'mime_header_checks':	value	=>'regexp:/etc/postfix/header_checks';
		'header_checks':	value	=>'regexp:/etc/postfix/header_checks';
	}	
	package {'swaks':
		ensure	=>	present,
		name	=>	'swaks',
	}
	package {'mailutils':
		ensure	=>	present,
		name	=>	'mailutils',
	}
	file {'header_checks':
		ensure	=>	present,
		path	=>	'/etc/postfix/header_checks',
		owner	=>	'root',
		group	=>	'root',
		mode	=>	'555',
		content =>	"/^Received:.*with ESMTPSA/ IGNORE\n/^X-Originating-IP:/ IGNORE\n/^X-Mailer:/ IGNORE\n/^Mime-Version:/ IGNORE\n",
	}
}
