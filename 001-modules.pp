class qz_modules {
  $modules= [
  	'example42-firewall',
        'example42-mariadb',
        'example42-stdmod',
        'puppetlabs-ntp',
        'akumria-nullmailer',
        'camptocamp-augeas',
        'camptocamp-postfix',
        'deric-accounts',
        'deric-gpasswd',
        'example42-iptables',
        'example42-puppi',
        'example42-resolver',
        'jfryman-nginx',
        'puppet-staging',
        'puppetlabs-apt',
        'puppetlabs-concat',
        'puppetlabs-mysql',
        'puppetlabs-stdlib',  ]


}
include qz_modules

