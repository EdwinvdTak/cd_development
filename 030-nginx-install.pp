class qz_nginxinstall {
        include apt
	apt::ppa { 'ppa:nginx/stable': }
	$packageList = [
		'nginx',
	]
	package { $packageList: 
		ensure	=>	present,
	}
	file {'create_fastcgi_cache_parent_dir':
		ensure	=>	'directory',
		path	=>	'/var/cache/nginx',
		owner	=> 	'www-data',
		group	=>	'www-data',
	}
	file {'create_fastcgi_cache_dir':
		ensure	=>	'directory',
		path	=>	'/var/cache/nginx/fastcgi_cache',
		owner	=> 	'www-data',
		group	=>	'www-data',
	}
	file_line{'edit_fstab': 
		path	=>	'/etc/fstab',
		line	=> 	'tmpfs /var/cache/nginx/fastcgi_cache tmpfs rw,size=64M,noatime,nodev,nosuid,noexec,uid=33,gid=33,mode=1700 0 0',
		match 	=>	'tmpfs /var/cache/nginx/fastcgi_cache tmpfs rw,size=64M,noatime,nodev,nosuid,noexec,uid=33,gid=33,mode=1700 0 0',
		match_for_absence	=>	true,
	}
	exec {'mount_all':
		command	=>	'/bin/mount -a',
		cwd	=>	'/tmp',
	}
	file {'nginx_default_config':
		ensure	=>	present,
		path	=>	'/etc/nginx/nginx.conf',
		source 	=>	'puppet:///storage/nginx/nginx.conf',
		owner 	=>	'root',
		group 	=>	'root',
		mode	=>	'750',
		
	}
	file {'nginx_log_dir_settings':
		ensure	=>	'directory',
		path	=>	'/var/log/nginx',
		owner 	=>	'www-data',
		group 	=>	'root',
		mode	=>	'750',
	}
	file {'make_debstyle_wsconfig_avail':
		ensure	=>	'directory',
		path	=>	'/etc/nginx/sites-available',
		owner 	=>	'root',
		group 	=>	'root',
		mode	=>	'750',
	}

	file {'make_debstyle_wsconfig_enabled':
		ensure	=>	'directory',
		path	=>	'/etc/nginx/sites-enabled',
		owner 	=>	'root',
		group 	=>	'root',
		mode	=>	'750',
	}
	if $ipaddress != '' {
		$listenipv4	=	"listen $ipaddress:80 default_server; "
	} else {
		$listenipv4	=	"listen :80 default_server \"\"; "
	}

	#undef<>""

	if $ipaddress != '' {
		$listenipv6	=	""
	} else {
		$listenipv6	=	"listen       [::]:80;"
	}
	
	file {'available_000_default_conf_file':
		path	=>	'/etc/nginx/sites-available/000-default.conf',
		owner	=> 	'root',
		group	=>	'root',
		mode	=>	'750',
		content	=>	join (['#',
		'# The default server/vhost',
		'#',
		'server {',
		'    # Stel het juiste IP adres',
		'    listen       127.0.0.1:80;',
		"$listenipv4",
		"$listenipv6",
		"    server_name  \"$hostname\" $ipaddress \"\"; ",
		'',
		'    #charset koi8-r;',
		'',
		'    #access_log  /var/log/nginx/access.log  main;',
		'    access_log off;',
		'    error_log   /var/log/nginx/error.log crit;',
		'',
		'    location / {',
		'        root   /usr/share/nginx/html;',
		'        index  index.html index.htm;',
		'    }',
		'',
		'    location /nginx_status {',
		'        stub_status on;',
		'        access_log   off;',
		'        allow 80.101.14.195;',
		'        allow 2001:980:24d8::/48;',
		'        allow 127.0.0.1;',
		'        deny all;',
		'    }',
		'',
		'    # Globale restricties voor alle WP vhosts',
		'    # include conf.d/restrictions.conf.inc;',
		'',
		'    error_page  404              /404.html;',
		'    location = /404.html {',
		'        root   /usr/share/nginx/html;',
		'    }',
		'',
		'    # redirect server error pages to the static page /50x.html',
		'    #',
		'    error_page   500 502 503 504  /50x.html;',
		'    location = /50x.html {',
		'        root   /usr/share/nginx/html;',
		'    }',
		'',
		'    # deny access to .htaccess files, if Apaches document root',
		'    # concurs with nginxs one',
		'    #',
		'    location ~ /\.ht {',
		'        deny  all;',
		'    }','}'],"\n"),
	}
	file {'/etc/nginx/sites-enabled/000-default.conf':
		ensure  =>	'link',
		target	=>	'/etc/nginx/sites-available/000-default.conf',
	}
	file {'install_upstream_conf':
		ensure	=>	'present',
		path	=>	'/etc/nginx/conf.d/upstream.conf.inc',
		content	=>	"upstream fastcgi {\n    server 127.0.0.1:9000;\n}\n\n",
		owner	=>	'root',
		group	=>	'root',
		mode	=>	'750',
	}
	file {'install_restrictions_conf':
		ensure	=>	'present',
		path	=>	'/etc/nginx/conf.d/restrictions.conf.inc',
		content =>	"# Global restrictions configuration file.\n# Designed to be included in any server {} block.\nlocation = /favicon.ico {\n        log_not_found off;\n        access_log off;\n}\n\nlocation = /robots.txt {\n        allow all;\n        log_not_found off;\n        access_log off;\n}\n\n# Deny all attempts to access hidden files such as .htaccess, .htpasswd, .DS_Store (Mac).\n# Keep logging the requests to parse later (or to pass to firewall utilities such as fail2ban)\nlocation ~ /\. {\n        deny all;\n}\n\n# Deny access to any files with a .php extension in the uploads directory\n# Works in sub-directory installs and also in multisite network\n# Keep logging the requests to parse later (or to pass to firewall utilities such as fail2ban)\nlocation ~* /(?:uploads|media|files|statics)/.*\.php$ {\n        deny all;\n}\n\n",
		owner	=>	'root',
		group	=>	'root',
		mode	=>	'750',
	}
	file {'install_tls_conf':
		ensure	=>	'present',
		path	=>	'/etc/nginx/conf.d/tls.conf.inc',
		content =>	"# Gedeelde TLS instellingen\nssl_prefer_server_ciphers On;\nssl_protocols TLSv1 TLSv1.2;\nssl_ciphers ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:DHE-RSA-AES128-SHA:AES128-SHA:DES-CBC3-SHA ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:DHE-RSA-AES128-SHA:AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:DES-CBC3-SHA;\n\n# DHE keysize verhogen van 1024 naar 2048 bits\nssl_dhparam /etc/ssl/certs/dhparam.pem;\n\nssl_session_cache shared:SSL:20m;\nssl_session_timeout 18h;\nssl_buffer_size 8k;\n\n# https://www.digitalocean.com/community/tutorials/how-to-configure-ocsp-stapling-on-apache-and-nginx\n# openssl s_client -connect cloudflare.com:443 -status (blok staat bovenaan, OCSP Response Status: successful || Cert Status: good)\nssl_stapling on;\nssl_stapling_verify on;\n#ssl_trusted_certificate /etc/ssl/certs/GeoTrust_RapidSSL_SHA2_CA-bundle.crt;\n",
		owner	=>	'root',
		group	=>	'root',
		mode	=>	'750',
	}
	service { 'nginx':
		      ensure => running,
		      enable => true,
    	}
}

