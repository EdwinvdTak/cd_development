class qz_github {
	file {'create_devops_dotssh':
		ensure	=> 	'directory',
		path 	=>	'/home/devops/.ssh',
		mode	=>	'600',
		owner	=>	'devops',
		group	=>	'devops',
	}
	file {'create_github_configfile':
		ensure	=>	present,
		path	=>	'/home/devops/.ssh/config',
		content => 	"Host github.com\n\tUser git\n\tIdentityFile /home/devops/.ssh/rsa-github_qz0_nl\n\n",
		mode    =>      '600',
		owner   =>      'devops',
		group   =>      'devops',
	}
	exec {'create_devops_github_key':
		provider	=>	shell,	
		onlyif	=> 	'test ! -f /home/devops/.ssh/rsa-github_qz0_nl',
		command	=>	'/usr/bin/ssh-keygen -t rsa -b 2024 -C "github@qz0-${HOSTNAME}-$(date +%Y-%m-%d)" -f /home/devops/.ssh/rsa-github_qz0_nl',
		creates	=>	'/home/devops/.ssh/rsa-github_qz0_nl.pub',
	}
	file {'create_aliases_extra':
		ensure  =>      'present',
		path    =>      '/home/devops/.bash_aliases',
		mode    =>      '600',
		owner   =>      'devops',
		group   =>      'devops',
		content	=>	'eval $(ssh-agent);ssh-add $HOME/.ssh/rsa-github_qz0_eu;',
	}
	file {'githubkey_rights':
		ensure  =>      'present',
		path    =>      '/home/devops/.ssh/rsa-github_qz0_nl',
		mode    =>      '600',
		owner   =>      'devops',
		group   =>      'devops',
	}
	file {'githubkey_rights_pub':
		ensure  =>      'present',
		path    =>      '/home/devops/.ssh/rsa-github_qz0_nl.pub',
		mode    =>      '600',
		owner   =>      'devops',
		group   =>      'devops',
	}
}