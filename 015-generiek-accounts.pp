#accounts
class qz_accounts {
#zorg dat de root user een lang password heeft 
#zorg dat devops user een lang password heeft 
#zorg dat de root ssh permitlogin without-password is 

#ssh-keygen -t ecdsa -b 521 -C "root@quadzero-$(date -I)" -f ~/.ssh/ecdsa-somo-qz0.nl_root

#maak voor root, qzops en devops een keypair aan
#zet qzops in de sudo group 
#zorg dat sudo een password vraagt in visudo 
	user { 'root':
		ensure => 'present',
		password	=>	'$6$ynXrmjXV$KiNV9B4y/iYJh64ise8t1K8uUWr2HuTWkPFfTtRqDOn/tNWkDzSG8f4lj9ALENhRCkZYgRrNAv3hMh.0hiuYJ/',
	}


	user { 'devops':
		ensure	=>	'present',
		gid	=>	'1000',
		home	=>	'/home/devops',
		password	=>	'$6$ynXrmjXV$KiNV9B4y/iYJh64ise8t1K8uUWr2HuTWkPFfTtRqDOn/tNWkDzSG8f4lj9ALENhRCkZYgRrNAv3hMh.0hiuYJ/',
		password_max_age	=>	'99999',
		password_min_age	=>	'0',
		shell	=>	'/bin/bash',
		uid	=>	'1000',
	}
	group { 'extra':
		name	=>	'extra',
		ensure	=>	'present',
		gid	=>	'1001',
	}

	user { 'qzops':
	        name	=>	'qzops',
		ensure	=>	'present',
		gid	=>	'1001',
		home	=>	'/home/qzops',
		password	=>	'',
		password_max_age	=>	'99999',
		password_min_age	=>	'0',
		shell	=>	'/bin/bash',
		groups	=>	'sudo',
		uid	=>	'1001',
	}
	
	file { 'qzops_homedir_fix':
		path	=>	'/home/qzops',
		ensure	=>	'directory',
		mode	=>	'700',
		owner	=>	'qzops',
	}
	
	ssh_authorized_key {'default_keys_qzops': 
		name	=>	'devops@quadzero-2016-03-23',
		ensure	=>	'present',
		key	=>	'A0AAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBAHAMcsAD76DO/J9ZLoXToikMgJMxIktE499a1KIBPNP8EWza7ovKLTUrUR/oMleWtSZGohWpcfv4ST8BBfWgUBBwgEPyR1VdlrwO5QX7onQ5BAYbDXRWshIgSRomM4DODCaYZ3ldNOKRb9wXNtqZPelYSUUrAAtfukmwZXnCHz0VA2gbA==',
		type	=>	'ecdsa-sha2-nistp521',
		user 	=>	'qzops',
	}
	ssh_authorized_key {'default_key_root': 
		name	=>	'devo2s@quadzero-2016-03-23',
		ensure	=>	'present',
		key	=>	'A1AAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBAHAMcsAD76DO/J9ZLoXToikMgJMxIktE499a1KIBPNP8EWza7ovKLTUrUR/oMleWtSZGohWpcfv4ST8BBfWgUBBwgEPyR1VdlrwO5QX7onQ5BAYbDXRWshIgSRomM4DODCaYZ3ldNOKRb9wXNtqZPelYSUUrAAtfukmwZXnCHz0VA2gbA==',
		type	=>	'ecdsa-sha2-nistp521',
		user 	=>	'root',
	}
	ssh_authorized_key {'default_key_devops': 
		name	=>	'devo3s@quadzero-2016-03-23',
		ensure	=>	'present',
		key	=>	'A2AAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBAHAMcsAD76DO/J9ZLoXToikMgJMxIktE499a1KIBPNP8EWza7ovKLTUrUR/oMleWtSZGohWpcfv4ST8BBfWgUBBwgEPyR1VdlrwO5QX7onQ5BAYbDXRWshIgSRomM4DODCaYZ3ldNOKRb9wXNtqZPelYSUUrAAtfukmwZXnCHz0VA2gbA==',
		type	=>	'ecdsa-sha2-nistp521',
		user 	=>	'devops',
	}
}