class qz_resolver {
	class { 'resolver':
  		dns_servers	=> [ '8.8.8.8' , '8.8.4.4' ],
		search	=> "$::domain qz0.nl",
		options	=> {
			'rotate'	=> '',
			'timeout'	=> '2',
		}
	}	
}